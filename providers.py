from abc import ABC, abstractmethod
import requests, json
from datetime import date, datetime
from os import path

def extract_hour(string):
	hour = int(string[0:2])
	hour += 12*("PM" in string)
	return str(hour)

def remove_diacritics(string):
	diacritics = "ąćęłńóśżź"
	substitutes = "acelnoszz"
	mapped = map(lambda c: substitutes[diacritics.index(c)] if c in diacritics else c, string)
	return "".join(list(mapped))

def imgw_standarize(string):
	'''Standarize string to be valid station name for IMGW API'''
	string = string.lower()
	string = remove_diacritics(string)
	string = string.replace(" ", "")
	string = string.replace("-", "")
	return string

class AbstractWeatherDataProvider(ABC):
	'''Abstract class describing methods of weather data provider.'''
	def __init__(self, name, city):
		self.name = name
		self.city = city
	
	def __update_temperature(self):
		self.temperature = self.get_temperature()
	
	def __update_wind_speed(self):
		self.wind_speed = self.get_wind_speed()
	
	def __update_humidity(self):
		self.humidity = self.get_humidity()
	
	def __update_pressure(self):
		self.pressure = self.get_pressure()
	
	def __update_date(self):
		self.date = self.get_date()
	
	def __update_hour(self):
		self.hour = self.get_hour()
	
	@abstractmethod
	def fetch_data(self):
		pass
	
	@abstractmethod
	def get_temperature(self):
		pass
	@abstractmethod
	def get_wind_speed(self):
		pass
	@abstractmethod
	def get_humidity(self):
		pass
	@abstractmethod
	def get_pressure(self):
		pass
	
	@abstractmethod
	def get_date(self):
		pass
	
	@abstractmethod
	def get_hour(self):
		pass
	
	def print_data():
		print("Currently saved %s data:" % name)
		print("--> Temperature = %s" % self.temperature)
	
	def __update_windchill_temp(self):
		self.windchill_temp = 13.12 + 0.6215*float(self.temperature) - 11.37*float(self.wind_speed)**0.16 
		self.windchill_temp += 0.3965*float(self.temperature)*float(self.wind_speed)**0.16
		self.windchill_temp = str(round(self.windchill_temp, 2))
	
	def update_all(self):
		'''Update all parameters of data provider.'''
		self.fetch_data()
		self.__update_temperature()
		self.__update_wind_speed()
		self.__update_humidity()
		self.__update_pressure()
		self.__update_hour()
		self.__update_date()
		self.__update_windchill_temp()
	def save_data_csv(self):
		headers = "Data provider,Date,Hour,City,Temperature [deg C],Windchill temperature [deg C],Pressure [hPa],Humidity [%],Wind speed [km/h]"
		filename = "weatherData.csv"
		file = None
		if path.isfile(filename):
			file = open(filename, "a")
		else:
			file = open(filename, "w+")
			file.write(headers)
		file.write("\n")
		file.write(",".join([
			self.name,
			self.date,
			self.hour,
			self.city,
			self.temperature,
			self.windchill_temp,
			self.pressure,
			self.humidity,
			self.wind_speed]))
		file.close()
		
	
class IMGWDataProvider(AbstractWeatherDataProvider):  

	def __init__(self, city):
		super().__init__("imgw", city)
		
	def fetch_data(self):
		URL_SYNOP = "https://danepubliczne.imgw.pl/api/data/synop"
		url = URL_SYNOP+"/station/" +imgw_standarize(self.city)
		response_str = requests.get(url).text
		parsed = json.loads(response_str)
		self.data = parsed
	
	def get_temperature(self):
		return self.data["temperatura"]
	
	def get_wind_speed(self):
		return str(
				round(
					int(
					self.data["predkosc_wiatru"])*36/10, 2))
	
	def get_humidity(self):
		return self.data["wilgotnosc_wzgledna"]
	
	def get_pressure(self):
		return self.data["cisnienie"]
	
	def get_date(self):
		return self.data["data_pomiaru"]
	
	def get_hour(self):
		return self.data["godzina_pomiaru"]
	
class WttrDataProvider(AbstractWeatherDataProvider):  
	
	def __init__(self, city):
		super().__init__("wttr", city)
		
	def fetch_data(self):
		url = "https://wttr.in/%s?format=j1" % (self.city)
		response_str = requests.get(url).text
		parsed = json.loads(response_str)
		self.data = parsed['current_condition'][0]
	
	def get_temperature(self):
		return self.data["temp_C"]
	
	def get_wind_speed(self):
		return self.data["windspeedKmph"]
	
	def get_humidity(self):
		return self.data["humidity"]
	
	def get_pressure(self):
		return self.data["pressure"]
	
	def get_date(self):
		return str(date.today())
	
	def get_hour(self):
		return extract_hour(self.data["observation_time"])
	


	