from datetime import date, datetime
from os import path
import pandas as pd
import os
import threading
import time

def max_with_index(coll):
	maximum = coll[0]
	max_index = 0
	for i in range (1, len(coll)):
		if coll[i] > maximum:
			maximum = coll[i]
			max_index = i
	return (maximum, max_index)

def min_with_index(coll):
	minimum = coll[0]
	min_index = 0
	for i in range (1, len(coll)):
		if coll[i] < minimum:
			minimum = coll[i]
			min_index = i
	return (minimum, min_index)

def print_min_max(df, column):
	"""Print extremums for given column in given df with time of appearence"""
	(_max, _max_index) = max_with_index(df[df.columns[column]])
	(_min, _min_index) = min_with_index(df[df.columns[column]])
	_max_hour = df[df.columns[2]][_max_index]
	_min_hour = df[df.columns[2]][_min_index]
	_max_date = df[df.columns[1]][_max_index]
	_min_date = df[df.columns[1]][_min_index]
	print("%s maximum of %s at hour %s of %s, minimum of %s at hour %s of %s" % 
		  (df.columns[column], _max, _max_hour, _max_date, _min, _min_hour, _min_date))
		  
def take_input():
	"""Waits for input and exits program if ENTER pressed"""
	value = input()
	if value == "" :
		print("Exiting")
		os._exit(1)
	return None
	
def wait(seconds):
	"""Wait given seconds and accept input while waiting"""
	print("")
	for i in range(seconds):
		print("\rContinuing in %d seconds... Press ENTER to interrupt" % (seconds-i), end="")
		t = threading.Thread(target=take_input)
		t.start()
		time.sleep(1)