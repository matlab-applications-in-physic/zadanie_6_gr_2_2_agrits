from providers import *
from datetime import date, datetime
from utils import max_with_index, min_with_index, print_min_max, wait, take_input
import pandas as pd
	

closest_cities = ["Katowice", "Częstochowa", "Racibórz"]
providers = []
for city in closest_cities:
	providers.append(IMGWDataProvider(city))
	providers.append(WttrDataProvider(city))

def main():
	"""The function updates weather data, saves it to csv and prints extremums."""
	print()
	for provider in providers:
		provider.update_all()
	for provider in providers:
		provider.save_data_csv()

	df = pd.read_csv('weatherData.csv', encoding = "ISO-8859-2")
	df[df.columns[4:8]] = df[df.columns[4:8]].astype(float)
	print("%s --> Current extremums:\n" % str(datetime.now()))
	for i in range(4,8):
		print_min_max(df, i)
	

		
while(True):
	main()
	wait(3600)
